import csv

def csv2listahtml(cvsname):
    nomes = []
    with open(cvsname) as file:
        cvshandler = csv.DictReader(file)

        #header = next(cvshandler)
        #print(header)

        for linha in cvshandler:
            #print(linha)
            nomes.append('{l[nome]} {l[sobrenome]}: {l[email]}'.format(l=linha))

        html_output = '<p>Segue a lista com {} nomes, extraidos do arquivo CVS {}:</p>'.format(len(nomes), cvsname)
        html_output += '\n<ul>'
        for i in sorted(nomes):
            html_output += '\n\t<li> {} </li>'.format(i)
        html_output += '\n</ul>'

    print(html_output)






if __name__ == '__main__':
    csv2listahtml('../massa_cvs/file.csv')
