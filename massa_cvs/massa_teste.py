import csv
import names
from rstr import rstr
from string import digits
import datetime
import re
from random import randint
from time import ctime


''' Gera um arquivo csv contendo os campos 
"cpf, matricula, sobrenome, nome, email, data de ingresso" 
'''

def gen_matricula():
    ''' ano + 5 digitos aleatorios'''
    while True:
        return '{}{}'.format(datetime.datetime.now().strftime('%Y'), rstr(digits, 5))

def get_matricula(data_ingresso=''):
    try:
        return '{}{}'.format(datetime.datetime.strptime(
            data_ingresso, "%a %b %d %H:%M:%S %Y").strftime("%Y"), rstr(digits, 5))
    except:
        return '{}{}'.format(datetime.datetime.now().strftime("%Y"), rstr(digits, 5))

def get_cpf():
    ''' retorna string de CPF não verificado'''
    while True:
        return '{}.{}.{}-{}'.format(
            rstr(digits, 3),
            rstr(digits, 3),
            rstr(digits, 3),
            rstr(digits, 2),
        )

def cpf_unformat(cpf):
    return ''.join(re.findall('\d+', cpf))

def cpf_format(cpf):
    for i in cpf:
        if i in '.-':
            return cpf
    return '{}.{}.{}-{}'.format(
        cpf[:3],
        cpf[3:6],
        cpf[6:9],
        cpf[-2:]
    )

def gen_nome_completo():
    '''retorna string com nome, sobrenome'''
    while True:
        yield (names.get_full_name())


def gen_email(nomecompleto, dominio='incolume.com.br'):
    '''recebe nome completo e dominio, retorna email'''
    try:
        return '{}.{}@{}'.format(*(nomecompleto.lower().split()), dominio)
    except:
        return None



def gen_data_ingresso():
    ''':return data'''
    seconds = int('{}{}'.format(
        randint(12, 14),
        rstr(digits, 8)
    ))
    return ctime(seconds)


def gen_massa(qlinhas, cvsname):
    '''cria cvsname com a quantidade de linhas informadas em qlinhas '''
    try:
        header = "cpf, matricula, sobrenome, nome, email, data de ingresso"
        with open(cvsname, 'w') as file:
            csvhandler = csv.writer(file)
            csvhandler.writerow(header.split(', '))
            for i in range(qlinhas):
                nome = gen_nome_completo()
                person = next(nome)
                date = gen_data_ingresso()
                linha = '{}, {}, {c[1]}, {c[0]}, {}, {}'.format(
                    get_cpf(),
                    get_matricula(date),
                    gen_email(nomecompleto=person),
                    date,
                    c = person.split(),
                    )
                csvhandler.writerow(linha.split(','))
        return True
    except:
        raise


if __name__ == '__main__':
    a = gen_nome_completo()

    for i in range(10):
        person = next(a)
        print('{}: {}'.format(person, gen_email(person)))

    print(gen_email(next(a)))
    print(gen_matricula())
    print(get_cpf())
    print(cpf_unformat(get_cpf()))
    print(cpf_format('12345678901'))
    print(cpf_format('123.456.789-01'))
    print(gen_data_ingresso())

    for i in range(5):
        "cpf, matricula, sobrenome, nome, email, data de ingresso"
        nome = gen_nome_completo()
        person = next(nome)
        print('{}, {}, {c[1]}, {c[0]}, {}, {}'.format(
            get_cpf(),
            gen_matricula(),
            gen_email(nomecompleto=person),
            gen_data_ingresso(),
            c = person.split(),
        ))


    print(gen_massa(30, 'file.csv'))
    print(get_matricula())
    print(get_cpf())
    print(gen_data_ingresso())
    print(get_matricula('Wed May 29 02:35:37 2013'))
    print(gen_email('Edward Aspinall'))
